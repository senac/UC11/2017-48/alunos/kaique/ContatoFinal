package com.example.kaiqueamoorim.projetofinalcontatos;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.Serializable;
import java.util.List;
import static com.example.kaiqueamoorim.projetofinalcontatos.R.id.listaContatos;
public class MainActivity extends AppCompatActivity {

    private ListView listView ;
    private ArrayAdapter<Contatos> contatosArrayAdapter ;
    private List<Contatos> lista ;
    private ContatoDAO dao ;



    private AdapterContatos adapterContatos;
    private ArrayAdapter<Contatos> adapter;

    public static final String CONTATOS = "contatos" ;

    private Contatos contatosSelecionados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        listView = findViewById(listaContatos);
        adapterContatos = new AdapterContatos(this,lista);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View contexto, int posicao, long indice) {

                contatosSelecionados = (Contatos) adapter.getItemAtPosition(posicao);

                Intent intent = new Intent(MainActivity.this,DetalhesActivity.class);

                intent.putExtra(CONTATOS, contatosSelecionados);

                startActivity(intent);
            }

        });
        registerForContextMenu(listView);




    }

    @Override
    protected void onResume() {
        super.onResume();


        listView = findViewById(R.id.listaContatos);


        dao = new ContatoDAO(this) ;
        lista = dao.getLista() ;
        dao.close();


        adapter = new ArrayAdapter<Contatos>(this , android.R.layout.simple_list_item_1 , lista) ;



        listView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu , menu);

        return true ;
    }

    public void adicionar(MenuItem item){

        Intent intent = new Intent(this , CadastroActivity.class);
        startActivity(intent);

    }
}
