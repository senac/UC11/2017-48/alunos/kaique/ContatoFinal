package com.example.kaiqueamoorim.projetofinalcontatos;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;


public class ContatoDAO extends SQLiteOpenHelper {



    private static final String DATABASE = "SQLite" ;
    private static final int VERSAO = 1 ;

    public ContatoDAO(Context context) {
        super(context, DATABASE , null, VERSAO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String ddl =  "CREATE TABLE COntatos " +
                "( id INTEGER PRIMARY KEY , " +
                "nome TEXT NOT NULL , " +
                "telefone TEXT  ,  " +
                "celular TEXT , " +
                "endereco TEXT) ; " ;

        db.execSQL(ddl);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

        String ddl  = "DROP TABLE IF EXISTS Contatos ;" ;
        db.execSQL(ddl);
        this.onCreate(db);

    }


    public void salvar(Contatos contatos ) {

        ContentValues values = new ContentValues() ;
        values.put("nome" , contatos.getNome());
        values.put("telefone" , contatos.getTelefone());
        values.put("celular" , contatos.getCelular());
        values.put("endereco" , contatos.getEndereço());



        getWritableDatabase().insert(
                "Contatos" ,
                null ,
                values) ;

    }


    public List<Contatos> getLista() {
        List<Contatos> lista = new ArrayList<>();
        // tem que tirar emaile site e nao conseguir se nao ele nao salva da erro ao salvar
        String colunas[] = {"id" , "nome" , "telefone" , "celular" , "endereco"} ;

        Cursor cursor =  getWritableDatabase().query(
                "Contatos",
                colunas,
                null,
                null,
                null,
                null,
                null);

        while(cursor.moveToNext()) {

            Contatos contatos = new Contatos();
            contatos.setId(cursor.getInt(0));
            contatos.setNome(cursor.getString(1));
            contatos.setTelefone(cursor.getString(2));
            contatos.setCelular(cursor.getString(3));
            contatos.setEndereço(cursor.getString(4));

            lista.add(contatos);

        }


        return lista ;
    }
}
