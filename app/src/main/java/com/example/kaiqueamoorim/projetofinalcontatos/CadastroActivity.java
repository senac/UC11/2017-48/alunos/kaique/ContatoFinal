package com.example.kaiqueamoorim.projetofinalcontatos;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CadastroActivity extends AppCompatActivity {
    private EditText txtNome ;
    private EditText txtTelefone ;
    private EditText txtCelular ;
    private EditText txtEndereco ;
    private Button btnSalvar ;

    private Contatos contatos ;
    private ContatoDAO dao ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        Intent intent = getIntent();
        contatos = (Contatos)intent.getSerializableExtra(MainActivity.CONTATOS);
        if (contatos==null){
            contatos = new Contatos();
        }else {
            preencherActivity(contatos);
        }




            txtNome = findViewById(R.id.txtNome);
            txtTelefone = findViewById(R.id.txtTelefone);
            txtCelular = findViewById(R.id.txtCelular);
            txtEndereco = findViewById(R.id.txtEndereco);
            btnSalvar = findViewById(R.id.btnSalvar);




            btnSalvar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    contatos = new Contatos();

                    contatos.setNome(txtNome.getText().toString());
                    contatos.setTelefone(txtTelefone.getText().toString());
                    contatos.setCelular(txtCelular.getText().toString());
                    contatos.setEndereço(txtEndereco.getText().toString());

                        dao = new ContatoDAO(CadastroActivity.this);
                        dao.salvar(contatos);
                        dao.close();
                        Toast.makeText(CadastroActivity.this ,
                                "Salvo com sucesso." ,
                                Toast.LENGTH_LONG).show();

                        finish();

                }
            });


        }

    private void preencherActivity(Contatos contatos) {

        txtNome.setText(contatos.getNome());
        txtTelefone.setText(contatos.getEndereço());
        txtCelular.setText(contatos.getCelular());
        txtEndereco.setText(contatos.getEndereço());
    }


}

