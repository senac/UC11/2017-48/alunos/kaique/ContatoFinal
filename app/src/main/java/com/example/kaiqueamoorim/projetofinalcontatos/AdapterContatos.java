package com.example.kaiqueamoorim.projetofinalcontatos;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class AdapterContatos extends BaseAdapter {
    private List<Contatos> lista;
    private Activity contexto;

    public AdapterContatos(Activity context, List<Contatos> lista){
        this.contexto = contexto;
        this.lista = lista;

    }
    @Override
    public int getCount() {
        return this.lista.size();
    }

    @Override
    public Object getItem(int position) {
        return this.lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        int posicao = 0;
        for (int i = 0 ; i < this.lista.size();i++){
            if(this.lista.get(i).getId() == posicao){
                posicao = i ;
                break;
            }
        }
        return posicao;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = contexto.getLayoutInflater().inflate(R.layout.activity_list__contatos_,parent,false);

        TextView textViewNome = view.findViewById(R.id.nome);


        Contatos contatos = this.lista.get(position);

        textViewNome.setText(contatos.getNome());

        return view;

    }
}
